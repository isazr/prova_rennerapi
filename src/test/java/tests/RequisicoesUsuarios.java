package tests;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import response.Esperado;

public class RequisicoesUsuarios {

	Esperado responseEsperado = new Esperado();
	String url = "https://reqres.in/api/users";

	@BeforeMethod
	public void AcessarAPI() {
		RestAssured.baseURI = url;
		System.out.println("Acessando a API..");
	}

	@Test
	public void buscarUsuarioID() {
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "2");

		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody, responseEsperado.getResponseBodyID());
		System.out.println("O usu�rio encontrado �: " + responseBody);

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		System.out.println("O statusCode � " + statusCode + "\n");

	}

	@Test
	public void criarUsuario() {
		RequestSpecification httpRequest = RestAssured.given();

		JSONObject parametro = new JSONObject();
		parametro.put("name", "morpheus");
		parametro.put("job", "leader");

		httpRequest.header("Content-Type", "application/json");

		httpRequest.body(parametro.toJSONString());

		Response response = httpRequest.request(Method.POST);
		String responseBody = response.getBody().asString();
		System.out.println("O usu�rio criado �: " + responseBody);

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 201);
		System.out.println("O statusCode � " + statusCode + "\n");

	}

	@Test
	public void validarListaUsuarios() {

		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "?page=2");

		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody, responseEsperado.getResponseBodyList());
		System.out.println("A lista de usu�rios cadastrados �: " + responseBody);

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		System.out.println("O statusCode � " + statusCode + "\n");

	}

	@Test
	public void updateInfoUsuario() {
		RequestSpecification httpRequest = RestAssured.given();

		JSONObject updateInfo = new JSONObject();
		updateInfo.put("name", "morpheus");
		updateInfo.put("job", "zion resident");

		httpRequest.header("Content-Type", "application/json");

		httpRequest.body(updateInfo.toJSONString());
		Response response = httpRequest.request(Method.PATCH, "4");
		String responseBody = response.getBody().asString();

		JsonPath novaInfo = response.jsonPath();
		String name = novaInfo.get("name");
		Assert.assertEquals(name, "morpheus");
		String job = novaInfo.get("job");
		Assert.assertEquals(job, "zion resident");
		System.out.println("Dados atualizados: " + responseBody);

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		System.out.println("O statusCode � " + statusCode + "\n");

	}

}
